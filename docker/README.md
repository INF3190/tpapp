# Docker pour TP INF3190


Installation et demarrages des services avec des contaners docker pour le developpement web du cours INF3190:

- Service "web"  (serveur apache avec php
- Service "db" (Serveur mysql)
-  Service phpmyadmin: Offre phpmyadmin pour l'administration de ba lase de données.

## Prerequis 
- Installer docker et docker compose

## Installer et demarrer les container

- Exécuter la commande suivante
  
~~~csh
docker compose up -d
~~~

>- Pour la premiere fois, va créer un dossier "data" (s'il n'existe pas) qui contiendra les données de mysql

- Pour spécifier le fichier docker-compose.yml

 ~~~csh
 docker compose -f docker-compose.yml up -d 
 ~~~


## Arrêter les services sans supprimer les containers docker (important pour garder ses config perso)

~~~csh
docker compose stop
~~~

## Démarrer les services arrêtés

~~~csh
docker compose start
~~~

## Arreter les services et supprimer les containers

~~~csh
docker compose down
~~~

## Accès site web

- Mettre vos pages web dans le dossier "web". Au besoin, créer des sous-dossiers. 
- Site permet l'exécution php
- site web: [http://localhost:4300](http://localhost:4300)
-  phpinfo:  [http://localhost:4300/phpinfo.php](http://localhost:4300/phpinfo.php) 

## accès phpmyadmin

- phpmyadmin : [http://localhost:4380](http://localhost:4380)
- Les comptes suivants correspondent à ceux créés par défaut sur le serveur bd avec la config de docker compose.
- serveur: db
- compte root:
  >- Utilisateur: root
  >- Mot de passe: INF3190root

- compte utilisateur normal:
  >- Utilisateur: etudiant
  >- Mot de passe: INF3190pass
  >- bd de l'utilisateur: tp
